<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="../resources/js/grid.locale-kr.js"></script>
<script type="text/javascript" src="../resources/js/jquery.jqGrid.js"></script>
<script type="text/javascript" src="../resources/js/jquery.serialize-object.min.js"></script>
<link rel="stylesheet" type="text/css" href="../resources/css/ui.jqgrid.css"/>
<style>
.ui-jqgrid {
position: fixed;

}

</style>

<script>
$.datepicker.setDefaults({
    dateFormat: 'yy-mm-dd' //Input Display Format 변경
});

function grid () {
	
	console.log("sabun" + $("#sabun").val());
	console.log("name" + $("#name").val());
	
	$.jgrid.gridUnload('#memberList');
	
	$("#memberList").jqGrid({
		url: '${ctx}'+'/member/memberSerch?name='+$("#name").val()+'&join_test='+$("#join_test").val()+'&put_yn='+$("#put_yn").val()
				+'&retire_test='+$("#retire_test").val()+'&sabun='+$("#sabun").val()+"&join_gbn_code="+$("#join_gbn_code").val()
				+'&job_type='+$("#job_type").val(),
		datatype : "json",
// 		loadonce:false,
// 		ajaxRowOptions: {
// 			contentType: "application/json;charset=UTF-8"
// 		},
        colModel: [
				{ name: 'sabun', label: '사번', align: 'center', width:1, hidden:true, key:true },
				{ name: 'sabun', label: '사번', align: 'center', frozen: true},
				{ name: 'name', label: '이름', align: 'center', frozen: true, editable: true },
				{ name: "reg_no", label: '주민번호', align: 'center'},
				{ name: "hp", label: '핸드폰번호', align: 'center',editable: true },
				{ name: "join_test", label: '입사일자', align: 'center' },
				{ name: "retire_test", label: '퇴사일자', align: 'center' }
        ],
        jsonReader: {
        	page: 'page',
        	total: 'total',
            records: 'records',
            root: 'memberList'
        },
        prmNames: {
            page: 'page',
            rows: 'size'
        }, 
        rownumbers: true,
        shrinkToFit: true,
        rowList:[5,10,20,30],
        pager: "#memberPager",
        viewrecords: true, 
        rowNum: 5,
        loadonce: false,
        width: $('div.row').innerWidth() - 80,
        onSelectRow : function(rowid) {
        	test(rowid);
        }
	});
}
function deleteMember(sabun) {
	var test = sabun;
	axios.get("${ctx}"+"/member/deleteMember?sabun="+sabun)
	.then(function(response) {
		$('.'+test).remove();
		alert("삭제되었습니다.");
	})
	.catch(function(error) {
		console.log(error);
		alert("삭제에 실패하였습니다. 관리자에게 문의바랍니다.")
	});
}
function test(sabun) {
	location.href="${ctx}"+"/member/update?sabun="+sabun;
}
function serch() {
	grid();
}

$(function(){
	$( "#mil_startdate, #mil_enddate, #join_test, #retire_test" ).datepicker();
	grid();
});

</script>
<body>
<div class="row">
	<form id="serchForm" name="serchForm">
		<div class="form-group">
			<div class="col-md-1">
				<span>사번</span>
			</div>
			<div class="col-md-2">
				<input id="sabun" name="sabun" type="text" value=""/>
			</div>
			
			<div class="col-md-1">
				<span>성명</span>
			</div>
			<div class="col-md-2">
				<input id="name" name="name" type="text" value=""/>
			</div>
			
			<div class="col-md-1">입사구분</div>
			<div class="col-md-2">
					<select id="insa_yn" style="width: 180px; height: 26px;">
						<option value="">전체</option>
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
			</div>
			
			<div class="col-md-1">투입여부</div>
			<div class="col-md-2">
					<select id="put_yn" name="put_yn" style="width: 180px; height: 26px;">
						<option value="">전체</option>
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-md-1">직위</div>
			<div class="col-md-2">
				<input type="text" id="join_gbn_code" name="join_gbn_code" value=""></input>
			</div>
			
			<div class="col-md-1">입사일자</div>
			<div class="col-md-2">
				<input id="join_test" name="join_test" type="text" value=""/>
				<i class="fa fa-calendar"></i>
			</div>
			<div class="col-md-1">퇴사일자</div>
			<div class="col-md-2">
				<input id="retire_test" name="retire_test" type="text" value=""/>
				<i class="fa fa-calendar"></i>
			</div>
			<div class="col-md-1">직종체크</div>
			<div class="col-md-2">
				<select id="job_type" name="job_type" style="width: 160px; height: 26px;" id="dept_code" name="dept_code">
					<option value="">전체</option>
					<option value="영업">영업</option>
					<option value="개발">개발</option>
				</select>
			</div>
			
		</div>
	</form>
</div>
<div>
	<input type="button" class="btn" onclick="serch();" value="검색" />
	<input type="button" class="btn" value="초기화" onclick="$('#serchForm')[0].reset();" />
	<input type="button" class="btn" value="이전" onclick="history.go(-1)" />
</div>

<div class="col-md-10">
		<table id="memberList" class="table table-hover scroll-body list-table list-collapse-table" summary="No,사번, 이름">
						
		</table>
		<div id ="memberPager"></div>
</div>
</body>
</html>