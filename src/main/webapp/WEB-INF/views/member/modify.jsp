<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>
.btn {
margin-bottom: 10px;
height: 30px;
}
img{
position: absolute; top:0; left: 0;
width: 100%;
height: 100%;
}
div{
padding-top: 5px;
}

</style>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery.serialize-object.min.js"></script>
<script>
$.datepicker.setDefaults({
    dateFormat: 'yy-mm-dd' //Input Display Format 변경
});

$(document).ready(function() {
	
	$("#mil_yn").on("change", function(){
		if ($("#mil_yn").val() == 'N') {
			$("select[name=mil_type]").val("");
			$("select[name=mil_type]").attr("disabled",true);
			$("select[name=mil_level]").val("");
			$("select[name=mil_level]").attr("disabled",true);
			$("#mil_startdate").attr("disabled", true);
			$("#mil_enddate").attr("disabled", true);
		} else {
			$("#mil_type").attr("disabled", false);
			$("#mil_level").attr("disabled", false);
			$("#mil_startdate").attr("disabled", false);
			$("#mil_enddate").attr("disabled", false);
		}
	});
	
	$(function() {
		$( "#mil_startdate, #mil_enddate, #join_test, #retire_test" ).datepicker();
		var test = '<c:out value = "${memberDTO.email}"/>';
		var a = test.length;
		var b = test.indexOf('@');
		
		$("#email1").val(test.substring(0,b));
		$("#email2").val(test.substring(b,a));
		
		console.log(test.substring(0,b));
		console.log(test.substring(b,a));
		
		//프로필 사진 업로드
		$("#profile").change(function(){
			var file = $(this).get(0).files[0];
			//사진유무체크
			fileCheck(file,1);

		});
		
	});
	
});

function testNumber (num) {
	
	if (num.id == 'reg_no') {
		if (num.value.length == 6) {
			num.value += " - ";
		}
	}
	
	else if (num.id == 'phone') {
		if (num.value.length == 3 || num.value.length == 9 ) {
			num.value += " - ";
		}
	} else {
		if (num.value.length == 3 || num.value.length == 10 ) {
			num.value += " - ";
		}
	}
}

function zipSearch() {
    new daum.Postcode({
      oncomplete: function(data) {
        // 각 주소의 노출 규칙에 따라 주소를 조합한다.
        // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
        var fullAddr = ''; // 최종 주소 변수
        var extraAddr = ''; // 조합형 주소 변수

        // 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
        if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
            fullAddr = data.roadAddress;

        } else { // 사용자가 지번 주소를 선택했을 경우(J)
            fullAddr = data.jibunAddress;
        }

        // 사용자가 선택한 주소가 도로명 타입일때 조합한다.
        if(data.userSelectedType === 'R'){
            //법정동명이 있을 경우 추가한다.
            if(data.bname !== ''){
                extraAddr += data.bname;
            }
            // 건물명이 있을 경우 추가한다.
            if(data.buildingName !== ''){
                extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
            }
            // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
            fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
        }

        // 우편번호와 주소 정보를 해당 필드에 넣는다.
        $('#visitRectZipCode').val(data.zonecode);
		$('#addr1').val(fullAddr);
        $('#addr2').focus();
      }
    }).open();
	};
/* 다음주소검색 api [e] */

function insertPass(test){
	
	var a = test.value;
	var c = "";
	
	if( a.length == 1) {
		test.nextSibling.nextSibling.value = a;
		return;
	}else if (a.length >= 2) {
		
		for (i=0; i < a.length-1; i++) {
			c = c+"*";
		}
		test.nextSibling.nextSibling.value += a.substring(a.length-1);
		c = c + a.substring(a.length-1);
		test.value = c;
		
	}
}

function UpdateMember() {
	$("#email").val($('email1').val()+$('email2').val());
	var dto = $('#testlist').serializeObject();
	
	axios.post("${ctx}/member/memberModify", dto)
	.then(function(response) {
		alert("수정 하였습니다.");
		MemberList();
	})
	.catch(function(error) {
		alert("수정에 실패하였습니다. 관리자에게 문의바랍니다.");
		console.log(error.response);
	});
}
function MemberList() {
	location.href='${ctx}'+'/member/memberList';
}

function deleteMember(sabun) {
	var test = sabun;
	axios.get("${ctx}"+"/member/deleteMember?sabun="+sabun)
	.then(function(response) {
		$('.'+test).remove();
		alert("삭제되었습니다.");
		MemberList();
	})
	.catch(function(error) {
		alert("삭제에 실패하였습니다. 관리자에게 문의바랍니다.")
	});
}

function fileCheck(file, flag){

	$.ajax({
		url: "${ctx}/member/fileCheck",
		dataType: "json",
        data: {"fileName":file.name},
		success: function(value){
			//값이 없을 때(업로드)
			if(!value){
				uploadImage(file, flag);
			}else{
				alert("동일한 파일이름이 존재합니다.");
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(textStatus);
		} 
	});
}

function uploadImage(file, flag){
	var data = new FormData();
	alert("호우");
	data.append('upload',file);
	
	$.ajax({
		url: "${ctx}/member/uploadPic",
		type: "post",
		dataType: "json",
		data: data,
		processData: false,
		contentType: false,
		success: function(json){
			console.log(json);
			if(flag == 1){
    			$("#profileImg").attr("src","../upload/"+json.src);
    			$(".profile").val(json.src);
			}else if(flag ==2){
				$("#regImg").val(json.src);
			}else{
				
				$("#carrierImg").val(json.src);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(textStatus);
		}
		
	}); 
}

</script>
<title>입사관리 시스템</title>
</head>
<body>
<div>
	<h2>직원 상세정보</h2></br>
	<div class="text-right">
		<button class="btn btn-primary" onclick='UpdateMember()'>수정</button>
		<button class="btn btn-primary" onclick="deleteMember(${memberDTO.sabun})">삭제</button>
		<button class="btn btn-primary" onclick="history.go(-1);">전화면</button>
	</div>
</div>
<form id="testlist" name="testlist">
<div>
	<div class="col-md-3" style="margin-left: 80px; margin-right: 45px; width:250px; height:190px; border:1px solid red; float:left;">
		<c:choose>
			<c:when test="${empty memberDTO.profile}">
				<img id="profileImg" src="https://www.ankarsrum.com/wp-content/uploads/2018/01/no-image-icon-.png"/></br>
			</c:when>
			<c:otherwise>
				<img id="profileImg" src="../upload/${memberDTO.profile }"/></br>
			</c:otherwise>
		</c:choose>
	</div>
	
	<div class="form-group col-md-9">
		<div class="row">
			<div class="col-md-1">사번</div>
			<div class="col-md-3" id="sabun_text">
				<input class="col-md-10" type="text" disabled="disabled" value="${memberDTO.sabun }"/>
				<input type="hidden" id="sabum" name="sabun" value="${memberDTO.sabun }"  />
			</div>
			<div class="col-md-1">한글성명</div>
			<div class="col-md-3">
				<input class="col-md-10" id="name" name="name" type="text" value="${memberDTO.name }" />
			</div>
			<div class="col-md-1">영문성명</div>
			<div class="col-md-3">
				<input class="col-md-10" id="eng_name" name="eng_name" type="text" value="${memberDTO.eng_name }" />
			</div>
			<div class="w-100"></div>
		</div>
		<div class="row">	
			<div class="col-md-1">아이디</div>
			<div class="col-md-3">
				<input class="col-md-10" type="text" id="id" name="id" value="${memberDTO.id}" />
			</div>
			<div class="col-md-1">패스워드</div>
			<div class="col-md-3">
				<input class="col-md-10" type="text" id="pwd_1" onkeyup="insertPass(this);" value="" />
				<input type="hidden" name="pwd" value="" />
			</div>
			<div class="col-md-1" style="padding :0">패스워드확인</div>
			<div class="col-md-3">
				<input class="col-md-10" type="text" id="pwd_c" onkeyup="insertPass(this);" value="" />
				<input type="hidden" id="pwd_c2" value = "" />
			</div>
			<div class="w-100"></div>
		</div>
	
		<div class="row">	
			<div class="col-md-1">전화번호</div>
			<div class="col-md-3">
				<input class="col-md-10" type="text" id="phone" name="phone" onkeyup="testNumber(this);" value="${memberDTO.phone }" />
			</div>
			<div class="col-md-1" style="padding :0">핸드폰번호</div>
			<div class="col-md-3">
				<input class="col-md-10" type="text" id="hp" name="hp" onkeyup="testNumber(this);" value="${memberDTO.hp }" />
			</div>
			<div class="col-md-1">주민번호</div>
			<div class="col-md-3">
				<input class="col-md-10" type="text" id="reg_no" name="reg_no" onkeyup="testNumber(this);" value="${memberDTO.reg_no }" />
			</div>
			<div class="w-100"></div>
		</div>
	
		<div class="row">	
			<div class="col-md-1">연령</div>
			<div class="col-md-1">
				<input class="col-md-10" type="text" id="years" name="years" value="${memberDTO.years }" />
			</div>
			<div class="col-md-1">이메일</div>
			<div class="col-md-2">
				<input class="col-md-10" type="text" id="email1" name="email1" value="" />
				<input type="hidden" id="email" name="email" value=/>
			</div>
			<div class="col-md-2">
				<select style="width: 160px; height: 26px;" id="email2" name="email2">
					<option value="">직접입력</option>
					<option value="@naver.com">@naver.com</option>
					<option value="@gmail.com">@gmail.com</option>
				</select>
			</div>
			<div class="col-md-1">직종체크</div>
			<div class="col-md-2">
				<select id="job_type" name="job_type" style="width: 160px; height: 26px;" id="dept_code" name="dept_code" value="${memberDTO.dept_code }">
					<option value="영입">영업</option>
					<option value="개발">개발</option>
				</select>
			</div>
			<div class="col-md-1" style="margin-right: -28px;">성별</div>
			<div class="col-md-1">
				<select id="sex" name="sex">
					<option value="M" <c:if test="${memberDTO.sex eq 'M'}"> selected </c:if>>남자</option>
					<option value="W" <c:if test="${memberDTO.sex eq 'W'}"> selected </c:if>>여자</option>
				</select>
			</div>
			<div class="w-100"></div>
		</div>
	
		<div class="row">	
			<div class="col-md-1">주소</div>
			<div class="col-md-2">
				<input class="col-md-10" type="text" id="visitRectZipCode" name="zip" value="${memberDTO.zip }" />
			</div>
			<div class="col-md-1">
				<input type="button" class="btn" onclick="zipSearch()" value="주소검색" />
			</div>
			<div class="col-md-4">
				<input class="col-md-10" type="text" id="addr1" name="addr1" value="${memberDTO.addr1 }"></input>
			</div>
			<div class="col-md-4">
				<input class="col-md-10" type="text" id="addr2" name="addr2" value="${memberDTO.addr2 }"></input>
			</div>
			<div class="w-100"></div>
		</div>
		
	</div>
	
	<div class="col-md-12">
		<div class="form-group">
			<div class="col-md-3 text-center">
				<input type="file" id="profile" />
				<input type="hidden" name="profile" class="profile" value="${memberDTO.profile }" />
			</div>
			<div class="col-md-1">직위</div>
			<div class="col-md-2">
				<input type="text" id="join_gbn_code" name="join_gbn_code" value="${memberDTO.join_gbn_code }"/>
			</div>
			<div class="col-md-1">부서</div>
			<div class="col-md-2">
				<input type="text" id="dept_code" name="dept_code" value="${memberDTO.dept_code }"/>
			</div>
			<div class="col-md-1">연봉(만원)</div>
			<div class="col-md-2 input-group">
				<input type="text" id="salary" name="salary" value="${memberDTO.salary }" />
			</div>
			<div class="w-100"></div>
		</div>
	
	</div>
	
	<div class="col-md-12">
		<div class="form-group">
			<div class="col-md-1">입사구분</div>
			<div class="col-md-2">
				<%-- if문으로 입사일자 퇴사일자 확인 후 선택자  --%>
				<select id="insa_YN" style="width: 180px; height: 26px;" value="${memberDTO.sabun }">
					<option value="Y">Y</option>
					<option value="N">N</option>
				</select>
			</div>
			<div class="col-md-1">등급</div>
			<div class="col-md-2">
				<select id="gart_level" name="gart_level" style="width: 180px; height: 26px;" ">
					<option value="" <c:if test = "${empty member.gart_level }"> selected </c:if>>(선택)</option>
					<option value="1" <c:if test = "${member.gart_level eq 1}"> selected </c:if>>1</option>
					<option value="2" <c:if test = "${member.gart_level eq 2}"> selected </c:if>>2</option>
					<option value="3" <c:if test = "${member.gart_level eq 3}"> selected </c:if>>3</option>
				</select>
			</div>
			<div class="col-md-1">투입여부</div>
			<div class="col-md-2">
				<select id="put_yn" name="put_yn" style="width: 180px; height: 26px;">
					<option value="Y" <c:if test = "${member.put_yn eq 'Y'}"> selected </c:if>>Y</option>
					<option value="N" <c:if test = "${member.put_yn eq 'N'}"> selected </c:if>>N</option>
				</select>
			</div>
			<div class="col-md-1">군필여부</div>
			<div class="col-md-2">
				<select id="mil_yn" name="mil_yn" style="width: 180px; height: 26px;" value="${memberDTO.mil_yn }">
					<option value="Y" <c:if test = "${member.mil_yn eq 'Y'}"> selected </c:if>>Y</option>
					<option value="N" <c:if test = "${member.mil_yn eq 'N'}"> selected </c:if>>N</option>
				</select>
			</div>
		</div>
	</div>
	
	
	<div class="col-md-12">	
		<div class="form-group">
			<div class="col-md-1">군별</div>
			<div class="col-md-2">
				<select id="mil_type" name="mil_type" style="width: 180px; height: 26px;" value="${memberDTO.mil_type }">
					<option value="">(선택)</option>
					<option value="육군" <c:if test = "${empty member.mil_type}"> selected </c:if>>육군</option>
					<option value="해군" <c:if test = "${member.mil_type eq '해군'}"> selected </c:if>>해군</option>
					<option value="공군" <c:if test = "${member.mil_type eq '공군'}"> selected </c:if>>공군</option>
				</select>
			</div>
			<div class="col-md-1">계급</div>
			<div class="col-md-2">
				<select id="mil_level" name="mil_level" style="width: 180px; height: 26px;">
					<option value="" <c:if test = "${empty member.mil_level}"> selected </c:if>>(선택)</option>
					<option value="병장" <c:if test = "${member.mil_level eq '병장'}"> selected </c:if>>병장</option>
					<option value="장교"<c:if test = "${member.mil_level eq '장교'}"> selected </c:if>>장교</option>
					<option value="부사관"<c:if test = "${member.mil_level eq '부사관'}"> selected </c:if>>부사관</option>
				</select>
			</div>
			<div class="col-md-1">입영일자</div>
			<div class="col-md-2">
				<input id="mil_startdate" name="mil_startdate" type="text" value="${memberDTO.mil_startdate }"/>
				<i class="fa fa-calendar"></i>
			</div>
			<div class="col-md-1">전역일자</div>
			<div class="col-md-2">
				<input id="mil_enddate" name="mil_enddate" type="text" value="${memberDTO.mil_enddate }"/>
				<i class="fa fa-calendar"></i>
			</div>
		</div>
	</div>
	
	<div class="col-md-12">	
		<div class="form-group">
			<div class="col-md-1">KOSA등록</div>
			<div class="col-md-2">
				<select id="kosa_reg_yn" name="kosa_reg_yn" style="width: 180px; height: 26px;">
					<option value="Y" <c:if test = "${memberDTO.kosa_reg_yn eq 'Y'}"> selected </c:if>>Y</option>
					<option value="N" <c:if test = "${memberDTO.kosa_reg_yn eq 'N'}"> selected </c:if>>N</option>
				</select>
			</div>
			<div class="col-md-1">KOSA등급</div>
			<div class="col-md-2">
				<select id="kosa_class_code" name="kosa_class_code" style="width: 180px; height: 26px;">
					<option value="1" <c:if test = "${memberDTO.kosa_class_code eq '1'}"> selected </c:if>>1</option>
					<option value="2" <c:if test = "${memberDTO.kosa_class_code eq '2'}"> selected </c:if>>2</option>
					<option value="3" <c:if test = "${memberDTO.kosa_class_code eq '3'}"> selected </c:if>>3</option>
				</select>
			</div>
			<div class="col-md-1">입사일자</div>
			<div class="col-md-2">
				<input id="join_test" name="join_test" type="text" value="${memberDTO.join_test }"/>
				<i class="fa fa-calendar"></i>
			</div>
			<div class="col-md-1">퇴사일자</div>
			<div class="col-md-2">
				<input id="retire_test" name="retire_test" type="text" value="${memberDTO.retire_test }"/>
				<i class="fa fa-calendar"></i>
			</div>
		</div>
	</div>
	
	<div class="col-md-12">	
		<div class="form-group">
			<div class="col-md-1">사업자 번호</div>
			<div class="col-md-2">
				<input id="cmp_reg_no" name="cmp_reg_no" type="text" value="${memberDTO.cmp_reg_no }"/>
			</div>
			<div class="col-md-1">업체명</div>
			<div class="col-md-2">
				<input id="crm_name" name="crm_name" type="text" value="${memberDTO.crm_name }"/>
			</div>
			<div class="col-md-1">사업자등록증</div>
			<div class="col-md-2">
				<input id="cmp_reg_image" name="cmp_reg_image" type="text" ${memberDTO.cmp_reg_image }/>
			</div>
			<div class="col-md-3">
				<button class="btn col-md-5" style="padding-bottom:10px;">미리보기</button>
				<span class="col-md-1"></span>
				<button class="btn col-md-5" style="padding-bottom:10px;">등록</button>
			</div>
		</div>
	</div>
	
	
	<div class="col-md-12">
		<div class="form-group">
			<div class="col-md-1">자기소개</div>
			<div class="col-md-5">
				<textarea id="self_intro" name="self_intro" style="resize : none; width: 92.2%;" maxlength="100">${memberDTO.self_intro }</textarea>
			</div>
			<div class="col-md-1">이력서</div>
			<div class="col-md-2">
				<input type="text"></input>
			</div>
			<div class="col-md-3">
				<button class="btn col-md-5">미리보기</button>
				<span class="col-md-1"></span>
				<button class="btn col-md-5">파일 업로드</button>
			</div>
		</div>
	</div>
</div>
</form>
</body>
</html>