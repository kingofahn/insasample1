<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="./test_include.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	.layer{position:absolute;top:0;left:0;width:100%;height:100%;text-align:center}
	.layer .content{display:inline-block;vertical-align:middle}
	.layer .blank{display:inline-block;width:0;height:100%;vertical-align:middle}
</style>
<script>

function MemberAdd() {
	alert("멤버 가입");
	location.href='${ctx}'+'/member/add';
}

function MemberList() {
	alert("멤버목록 조회");
	location.href='${ctx}'+'/member/memberList';
}
// $('#register').click(function(){
// 	alert("멤버 가입");
// 	location.href='${ctx}'+'/member/add'
// })
</script>
</head>
<body class="bg-primary">
	<div style="text-align: center">
		<span style="text-align: left">IT & BIZ</span>
		<span style="cursor:pointer;">Home</span>
		<span style="cursor:pointer;">input</span>
		<span style="cursor:pointer;">Search</span>
	</div>
	<div class="layer">
		<span class="content">
			<h3>인사관리 시스템</h3>
			인사정보를 입력하겠습니다.<button onclick="MemberAdd()" class="btn btn-primary">저장</button></br></br>
			인사정보를 조회하겠습니다.<button onclick="MemberList()" class="btn">조회</button></br></br>
		</span>
		<span class="blank"></span>
	</div>

</body>
</html>