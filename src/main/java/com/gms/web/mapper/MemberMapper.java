package com.gms.web.mapper;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.gms.web.domain.MemberDTO;
@Repository
public interface MemberMapper {
	public void insert(MemberDTO p);
	public List<MemberDTO> memberList(HashMap<String, Object> test) ;
	public MemberDTO selectOne(int p) ;
	public void update(MemberDTO p) ;
	public void delete(int p) ;
	public int maxSabun();
	public List<MemberDTO> test(MemberDTO p);
	public MemberDTO selectImg(String fileName);
}
