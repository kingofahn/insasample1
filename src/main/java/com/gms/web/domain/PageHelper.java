package com.gms.web.domain;

public class PageHelper {
	private int startRow;
	private int endRow;
	
	public PageHelper(int page, int size) {
		if (page <= 1) {
			this.startRow = 0;
		} else {
			this.startRow = (page*size)-size;
		}
		
		this.endRow = page*size;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

}
