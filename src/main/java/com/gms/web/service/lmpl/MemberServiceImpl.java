package com.gms.web.service.lmpl;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gms.web.domain.MemberDTO;
import com.gms.web.mapper.MemberMapper;
import com.gms.web.service.MemberService;

@Service
public class MemberServiceImpl implements MemberService {
	@Autowired MemberMapper memberDAO;
	@Override
	public void add(MemberDTO p) throws Exception{
		p.setSabun(memberDAO.maxSabun()+1);
		
		if (!p.getJoin_test().isEmpty()) {
			p.setJoin_day(new SimpleDateFormat("yy-MM-dd").parse(p.getJoin_test()));
		}
		if (!p.getRetire_test().isEmpty()) {
			p.setRetire_day(new SimpleDateFormat("yy-MM-dd").parse(p.getRetire_test()));
		}
		memberDAO.insert(p);
	}
	@Override
	public List<MemberDTO> memberList(HashMap<String, Object> test) {

		List<MemberDTO> list = memberDAO.memberList(test);
		
		System.out.println(list.size());
		
		for (int i = 0; i<list.size(); i++) {
			MemberDTO dto = list.get(i);
			if (dto.getJoin_day() != null) {
				dto.setJoin_test(new SimpleDateFormat("yy-MM-dd").format(dto.getJoin_day()));
			}
			if (dto.getRetire_day() != null) {
				dto.setRetire_test(new SimpleDateFormat("yy-MM-dd").format(dto.getRetire_day()));
			}
			list.set(i, dto);
		}
		
		return list;
	}
	
	@Override
	public MemberDTO retrieve(int p) {
		return memberDAO.selectOne(p);
	}
	@Override
	public void modify(MemberDTO p) {
		memberDAO.update(p);
	}
	@Override
	public void remove(int p) {
		memberDAO.delete(p);
		
	}
	@Override
	public List<MemberDTO> test(MemberDTO p) {
		
		return  memberDAO.test(p);
	}
	@Override
	public boolean checkProfile(String fileName) {
		MemberDTO dto = memberDAO.selectImg(fileName);
		
		if(dto != null) {
			return true;
		}else {
			return false;
		}
	}
}