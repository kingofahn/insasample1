package com.gms.web.service;

import java.util.HashMap;
import java.util.List;

import com.gms.web.domain.MemberDTO;

public interface MemberService {
	public void add(MemberDTO p) throws Exception ;
	public List<MemberDTO> memberList(HashMap<String, Object> test) ;
	public MemberDTO retrieve(int p) ;
	public void modify(MemberDTO p) ;
	public void remove(int p) ;
	public List<MemberDTO> test(MemberDTO p);
	public boolean checkProfile(String fileName);

}
