package com.gms.web.controller;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.gms.web.domain.MemberDTO;
import com.gms.web.domain.PageData;
import com.gms.web.domain.PageHelper;
import com.gms.web.service.MemberService;
import com.gms.web.util.ResizeImageUtil;
@Controller
/*@SessionAttributes("user")*/
public class MemberController {
	
	private MemberService service;
	
	@RequestMapping("member/add")
	public String memberAdd(){
		return "MemberAdd.tiles";
	}
	
	@RequestMapping(value="member/memberList")
	public String getList() {
		
		return "MemberList.tiles";
	}
	
	@RequestMapping(value="member/update", method = RequestMethod.GET)
	public String memberUpdate(@RequestParam("sabun") int sabun, Model model, 
			final HttpServletResponse response, final HttpServletRequest request) {
		
		MemberDTO dto = service.retrieve(sabun);
		if (dto.getJoin_day() != null) {
			dto.setJoin_test(new SimpleDateFormat("yy-MM-dd").format(dto.getJoin_day()));
		}
		if (dto.getRetire_day() != null) {
			dto.setRetire_test(new SimpleDateFormat("yy-MM-dd").format(dto.getRetire_day()));
		}
		
		model.addAttribute("sabun", sabun);
		model.addAttribute("memberDTO", dto);
		
		return "MemberUpdate.tiles";
	}
	
	@RequestMapping(value="member/insertMember", method = {RequestMethod.POST})
	public ResponseEntity<Void> insertMember(@RequestBody final MemberDTO dto) throws Exception{
		service.add(dto);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "member/deleteMember", method = RequestMethod.GET)
	public ResponseEntity<Void> deleteMember(@RequestParam("sabun")int p) {
		service.remove(p);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="member/memberSerch" ,  method = { RequestMethod.GET })
	public ResponseEntity<PageData> getMember(@RequestParam(value = "page", defaultValue = "0") final int page,
			  @RequestParam(value = "size", defaultValue = "20") final int size, @RequestParam(value="name", defaultValue ="") final String name,
			  @RequestParam(value = "join_test", defaultValue ="") final String join_test, @RequestParam("put_yn") final String put_yn,
			  @RequestParam(value = "retire_test", defaultValue ="") final String retire_test, @RequestParam(value="job_type", defaultValue ="") final String job_type,
			  @RequestParam(value = "sabun", defaultValue ="0") final int sabun, @RequestParam(value = "join_gbn_code") String join_gbn_code) throws Exception{
		
		PageHelper pageHelper = new PageHelper(page, size);

		HashMap<String, Object> test = new HashMap<String, Object>();
		test.put("name", name);
		test.put("sabun", sabun);
		if (!join_test.equals("")) {
			test.put("join_day", new SimpleDateFormat("yy-MM-dd").parse(join_test));
		} else {
			test.put("join_day", null);
		}
		if (!retire_test.equals("")) {
			test.put("retire_day", new SimpleDateFormat("yy-MM-dd").parse(retire_test));
		} else {
			test.put("retire_day", null);
		}
		test.put("put_yn", put_yn);
		test.put("job_type", job_type);
		test.put("startRow", pageHelper.getStartRow());
		test.put("endRow", pageHelper.getEndRow());		
		
		List<MemberDTO> memberList = service.memberList(test); 
		
		int totalPage;
		
		if ( memberList.size() != 0){
			totalPage = memberList.get(0).getTotal()/size+1;
		} else {
			totalPage = 0;
		}
		
		PageData testPage = new PageData();
		testPage.setMemberList(memberList);
		testPage.setTotal(totalPage);
		testPage.setPage(page);
		testPage.setRecords(memberList.size());
		
		
		
		return new ResponseEntity<>(testPage, HttpStatus.OK);
	}
	
	@RequestMapping(value="member/test" ,  method = { RequestMethod.POST })
	public ResponseEntity<List<MemberDTO>> test(@RequestBody MemberDTO p) throws Exception {
		
		if (!p.getJoin_test().isEmpty()) {
			p.setJoin_day(new SimpleDateFormat("yy-MM-dd").parse(p.getJoin_test()));
		}
		if (!p.getRetire_test().isEmpty()) {
			p.setRetire_day(new SimpleDateFormat("yy-MM-dd").parse(p.getRetire_test()));
		}
		
		List<MemberDTO> memberList = new ArrayList<MemberDTO>();
		memberList = service.test(p);
		if (memberList.size() > 0) {
			for (int i = 0; i < memberList.size(); i++) {
				MemberDTO dto = memberList.get(i);
				
				if (dto.getJoin_day() != null) {
					dto.setJoin_test(new SimpleDateFormat("yy-MM-dd").format(dto.getJoin_day()));
				}
				if (dto.getRetire_day() != null) {
					dto.setRetire_test(new SimpleDateFormat("yy-MM-dd").format(dto.getRetire_day()));
				}
				
				memberList.set(i, dto);
			}
		}
		
		return new ResponseEntity<>(memberList, HttpStatus.OK);
	}
	
	@RequestMapping(value="member/memberModify", method = RequestMethod.POST)
	public ResponseEntity<String> memberModify(@RequestBody MemberDTO p) throws Exception{
		
		System.out.println(p.getName());
		if (!p.getJoin_test().isEmpty()) {
			p.setJoin_day(new SimpleDateFormat("yy-MM-dd").parse(p.getJoin_test()));
		}
		if (!p.getRetire_test().isEmpty()) {
			p.setRetire_day(new SimpleDateFormat("yy-MM-dd").parse(p.getRetire_test()));
		}
		service.modify(p);
		
		return new ResponseEntity<> ("MemberList.tiles", HttpStatus.OK);
	}
	
	//사진업로드
	@RequestMapping(value="member/uploadPic", method=RequestMethod.POST)
	@ResponseBody
	public String uploadPic(MultipartFile upload,HttpServletRequest request) throws Exception{
		// 1) ServletContext얻기
		ServletContext sc = request.getServletContext();
		System.out.println(sc.getContextPath());

		// 2) 기본경로 얻기
		String path = "C://Users//admin//Desktop//sample//src//main//webapp//";
		System.out.println("리얼패스" + sc.getRealPath(""));

		// 3) upload경로
		String uploadPath = path + "thumbs" + File.separator;
		System.out.println("업로드 패스" + uploadPath);
		// 4)  image경로
		String resizePath = path + "upload" + File.separator;
		System.out.println("경로" + resizePath);
		
		String fileName = upload.getOriginalFilename();

		System.out.println(resizePath);
		
		System.out.println(fileName);

		// 6) 경로+파일이름
		String fullPath = uploadPath + fileName;

		// 7) 실제 생성될 파일
		File file = new File(fullPath);

		System.out.println(file);
		// 8) 파일 옮기기
		upload.transferTo(file);
		System.out.println("파일 생성 성공");

		
		ResizeImageUtil.resize(fullPath, resizePath + fileName, 200, 200);
		return "{\"src\":\"" + fileName + "\"}";
	}
	
	//파일삭제
	@RequestMapping(value="/member/deletePic", method=RequestMethod.DELETE)
	@ResponseBody
	public void deletePic(String name,HttpServletRequest request) {
		// 1) ServletContext얻기
		ServletContext sc = request.getServletContext();
		// 2) 기본경로 얻기
		String path = sc.getRealPath("");

		// 3) upload경로
		String uploadPath = path + "upload" + File.separator;

		// 4)  image경로
		String resizePath = path + "thumbs" + File.separator;
		
		  try {
			    File fileEx = new File(uploadPath);
			    File fileEx1 = new File(resizePath);
			    if (fileEx.exists()) {fileEx.delete();}
			    if (fileEx1.exists()) {fileEx.delete();}
			    System.out.println("삭제완료");
		  } catch (Exception e) {
		    	e.printStackTrace();

		  }
	}

//	DB에 중복파일 체크
	@RequestMapping(value="member/fileCheck", method=RequestMethod.GET)
	@ResponseBody
	public boolean fileCheck(String fileName) {
		//System.out.println(fileName);

		return service.checkProfile(fileName);
	}
	
	@Autowired
	public void setMemberService(MemberService service) {
		this.service = service;
	}
	
}